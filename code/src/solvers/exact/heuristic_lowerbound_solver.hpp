#pragma once

#include "../../graph/bipartite_graph/bip_graph.hpp"
#include "../solver_result.hpp"
#include "../../evaluator/evaluator.h"
#include "heuristic_lowerbound_solver.hpp"
#include "../../lower_bounds/costs_lb.hpp"
#include "../../graph/order_graph.hpp"
#include "../../graph/digraph/utilities.hpp"
#include "../heuristic/local_improve.hpp"

/**
 * Tries to solve the problem with solvers and ultimately tries to
 * make order given by the lower bound. If nothing succeeds (i.e. we dont get an order with matching lower bound)
 * this throws runtime_error
 * @param solvers - solvers (=function) with signature `solver_result(const bip_graph&)` to be tried
 * @return exact solution on success
 */
solver_result hl_solve(const bip_graph& G, auto&& ... solvers)
{
    auto costs = costs::compute(G, interval_info::compute(G));
    
    //run lower_bound
    auto lb = costs_lower_bound(costs);
    
    //otherwise, just run the heuristics
    solver_result best = {.order = G.get_free_part_as<std::vector>()}; //initialize with something random
    long long best_score = evaluate(G, best);
    
    auto try_solver = [&](auto&& solver) -> bool //returns true if lb is matched
    {
        auto res = solver(G);
        //local_improve(res,G);
        if (auto ev = evaluate(G, res); ev < best_score) {
            best_score = ev;
            best = std::move(res);
        }
        return best_score == lb;
    };
    
    if((try_solver(solvers) || ...))
        return best;
    
    //try to make order graph from lower bound
    auto order_graph = make_order_graph(costs);
    //if the order graph is acyclic, try to make an order from the lower bound
    if (auto ord = topsort(order_graph); ord != std::nullopt) {
        solver_result res = {.order = *ord};
        if (auto ev = evaluate(G, res); ev < best_score) {
            best_score = ev;
            best = std::move(res);
        }
    }
    
    if (best_score == lb)
        return best;
    throw std::runtime_error("No exact solution found.");
}