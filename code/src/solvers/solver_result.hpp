#pragma once
#include <iostream>
#include "../graph/bipartite_graph/bip_graph.hpp"

struct solver_result
{
    std::vector<vertex_t> order;
    
    /**
     * Checks whether this result is a valid result corresponding to some solution of ... for graph g
     */
    [[nodiscard]] bool is_valid_result_of(const bip_graph& g) const;
};

std::ostream& operator<<(std::ostream& os, const solver_result& res);
