#include "solver_result.hpp"

bool solver_result::is_valid_result_of(const bip_graph& g) const
{
    return permutation(order).is_permutation_of(g.get_free_part_as<std::vector>());
}

std::ostream& operator<<(std::ostream& os, const solver_result& res)
{
    for (auto&& v: res.order)
        os << v << '\n';
    return os;
}