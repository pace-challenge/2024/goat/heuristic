#include "heuristic_solver.hpp"
#include "../heuristic/baryc_solver.hpp"
#include "../heuristic/median_solver.hpp"
#include "../../graph/bipartite_graph/bip_graph_io.hpp"

int main()
{
    std::cout << heuristic_solve(load_bip_graph_pace(std::cin), median_solve, baryc_solve_def);
}