#include "../../graph/bipartite_graph/bip_graph.hpp"
#include "../../evaluator/evaluator.h"

inline void local_improve(solver_result& solution, const bip_graph& g)
{
    bool changed = true;
    long long opt = evaluate(g, solution);
    size_t max_iters = 1000000L / (g.free().size());
    std::cerr << "max_iters= " << max_iters << "\n";
    size_t iter = 0;
    while (changed && iter < max_iters) {
        changed = false;
        for (int i = 0; i < (int)solution.order.size(); ++i) {
            for(auto x : {1,2,3,-1,-2,-3})
                if(i + x >= 0 && i + x < (int)solution.order.size())
                {
                    std::swap(solution.order[i], solution.order[i + x]);
                    if (auto ev = evaluate(g, solution); ev < opt) {
                        std::cerr << "improved solution by " << opt - ev << "\n";
                        opt = ev;
                        changed = true;
                    }
                    else
                        std::swap(solution.order[i], solution.order[i + x]);
                }
        }
        iter++;
    }
    std::cerr << "opt= " << opt << "\n";
}