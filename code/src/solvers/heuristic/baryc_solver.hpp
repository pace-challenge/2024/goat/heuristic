#pragma once

#include <type_traits>
#include "../solver_result.hpp"


template <typename T, typename G>
concept VertexMapper =requires(T map, const G& g)
{
    { map(g) } -> std::convertible_to<std::unordered_map<vertex_t, double>>;
};

struct uniform
{
    std::unordered_map<vertex_t, double> operator()(const bip_graph& g)
    {
        auto&& X_perm = g.get_fixed_permutation();
        if (X_perm.size() == 1)
            return {{X_perm[0], 0.0}};
        
        std::unordered_map<vertex_t, double> ret;
        
        double step = 1. / (double) (X_perm.size() - 1);
        double y = 0;
        for (auto& v: X_perm) {
            ret[v] = y;
            y += step;
        }
        return ret;
    }
};

template <VertexMapper<bip_graph> Mapper = uniform>
solver_result baryc_solve_impl(const bip_graph& G, Mapper map = Mapper())
{
    auto X_pos = map(G);
    std::unordered_map<vertex_t, double> Y_pos;
    for (auto& [y, adj]: G.free()) {
        if (adj.empty()) {
            Y_pos[y] = 0; //isolated vertices are anywhere
            continue;
        }
        double sum = 0.0;
        for (auto& x: adj)
            sum += X_pos[x];
        Y_pos[y] = sum / (double) adj.size();;
    }
    return solver_result {.order = to_ord(Y_pos)};
}

solver_result baryc_solve_def(const bip_graph& G)
{
    return baryc_solve_impl(G,uniform());
}