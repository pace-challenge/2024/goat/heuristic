#include "median_solver.hpp"
#include "../../graph/bipartite_graph/bip_graph_io.hpp"

int main()
{
    std::cout << median_solve(load_bip_graph_pace(std::cin));
    return 0;
}