#pragma once

#include "../../evaluator/evaluator.h"
#include "../solver_result.hpp"
#include "local_improve.hpp"
/**
 * Tries to solve with several solvers, returns the best result.
 */
solver_result heuristic_solve(const bip_graph& G, auto&& ... solvers)
{
    solver_result best = {.order = G.get_free_part_as<std::vector>()};
    long long best_score = evaluate(G, best);
    
    auto try_solver = [&](auto&& solver)
    {
        auto res = solver(G);
        //local_improve(res,G);
        if (auto ev = evaluate(G, res); ev < best_score) {
            best_score = ev;
            best = std::move(res);
        }
    };
    (try_solver(solvers), ...);
    return best;
}