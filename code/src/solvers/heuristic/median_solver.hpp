#include "../../graph/bipartite_graph/bip_graph.hpp"
#include "../solver_result.hpp"

solver_result median_solve(const bip_graph& G)
{
    std::unordered_map<vertex_t, size_t> Y_pos;
    for (auto& [y, adj]: G.free()) {
        if (adj.empty()) //isolated vertices will be anywhere ...
        {
            Y_pos[y] = 0;
            continue;
        }
        std::vector<size_t> positions;
        for (auto& x: adj)
            positions.push_back(G.get_fixed_permutation().pos(x));
        std::nth_element(positions.begin(), positions.begin() + positions.size() / 2, positions.end());
        Y_pos[y] = positions[positions.size() / 2];
    }
    return solver_result {.order = to_ord(Y_pos)};
}