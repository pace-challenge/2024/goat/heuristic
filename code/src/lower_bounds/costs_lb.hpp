#pragma once

#include "../graph/bipartite_graph/bip_graph.hpp"
#include "../graph/interval_info.hpp"
#include "../graph/costs.hpp"


inline long long costs_lower_bound(const costs& cost_structure)
{
    long long lb = 0;
    auto&& c = cost_structure.c;
    for (auto&& [pr, cost]: c) {
        auto&& [u, v] = pr;
        assert(c.contains({v, u}));
        lb += std::min(c.at({v, u}), c.at({u, v}));
    }
    assert(!(lb & 1));
    return lb / 2; //each costs gets counted twice
}


inline long long costs_lower_bound(const bip_graph& g)
{
    return costs_lower_bound(costs::compute(g, interval_info::compute(g)));
}

inline long long costs_lower_bound_naive(const bip_graph& g)
{
    long long lb = 0;
    for (auto&& [u, adju]: g.free())
        for (auto&& [v, adjv]: g.free()) {
            if (u == v)
                continue;
            lb += std::min(g.num_crossings(u, v), g.num_crossings(v, u));
        }
    assert(!(lb & 1));
    return lb / 2;
}