#include <iostream>
#include "costs_lb.hpp"
#include "../graph/bipartite_graph/bip_graph_io.hpp"

int main()
{
    std::cout << costs_lower_bound(load_bip_graph_pace(std::cin)) << std::endl;
}