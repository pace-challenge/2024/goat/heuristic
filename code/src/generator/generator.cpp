#include "generator.hpp"
#include "../graph/bipartite_graph/graph_builder.hpp"

bip_graph_generator::bip_graph_generator(unsigned long seed) : rng {seed} { }

bip_graph bip_graph_generator::gen_graph(size_t fixed_size, size_t free_size, size_t num_edges)
{
    graph_builder gb;
    std::vector<vertex_t> fixed(fixed_size), free(free_size);
    std::iota(fixed.begin(), fixed.end(), 1);
    std::iota(free.begin(), free.end(), fixed_size + 1);
    gb.set_free_part(std::move(free));
    gb.set_fixed_part(std::move(fixed));
    if (num_edges > fixed_size * free_size)
        throw std::invalid_argument("Too many edges");
    size_t e = 0;
    while (e != num_edges) {
        vertex_t u = (vertex_t) _random_number(1, fixed_size);
        vertex_t v = (vertex_t) _random_number(fixed_size + 1, fixed_size + free_size);
        if (gb.add_edge(u, v))
            ++e;
    }
    return gb.build();
}

void bip_graph_generator::seed(unsigned long seed)
{
    rng.seed(seed);
}

unsigned long bip_graph_generator::_random_number(unsigned long min, unsigned long max)
{
    assert(min <= max);
    return min + rng() % (max - min + 1);
}
