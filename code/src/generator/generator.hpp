#pragma once

#include <random>
#include "../graph/bipartite_graph/bip_graph.hpp"


struct bip_graph_generator
{
    explicit bip_graph_generator(unsigned long seed = 42);
    
    void seed(unsigned long seed);
    
    [[nodiscard]] bip_graph gen_graph(size_t fixed_size, size_t free_size, size_t num_edges);
    
    
    [[nodiscard]] auto& get_rng() { return rng; }
  
  private:
    unsigned long _random_number(unsigned long min, unsigned long max);
    
    std::mt19937 rng;
};
