#include "segtree.hpp"

seg_tree::seg_tree(size_t n) : n_(n),
                               tree_(4 * n, 0) { }

long long seg_tree::sum(size_t l, size_t r)
{
    return _sum(l, r, 0, n_ - 1, 1);
}

void seg_tree::increment(size_t i)
{
    _increment(i, 0, n_ - 1, 1);
}

size_t seg_tree::size() const
{
    return n_;
}

constexpr auto LEFT(auto v)
{
    return 2 * v;
}

constexpr auto RIGHT(auto v)
{
    return 2 * v + 1;
}

long long seg_tree::_sum(size_t lq, size_t rq, size_t ln, size_t rn, size_t v)
{
    if (lq > rq)
        return 0;
    if (lq == ln && rq == rn)
        return tree_[v];
    auto mid = ln + (rn - ln) / 2;
    return _sum(lq, std::min(rq, mid), ln, mid, LEFT(v))
           +
           _sum(std::max(lq, mid + 1), rq, mid + 1, rn, RIGHT(v));
}

void seg_tree::_increment(size_t i, size_t ln, size_t rn, size_t v)
{
    if (ln == rn)
        ++tree_[v];
    else {
        auto mid = ln + (rn - ln) / 2;
        if (i <= mid)
            _increment(i, ln, mid, LEFT(v));
        else
            _increment(i, mid + 1, rn, RIGHT(v));
        tree_[v] = tree_[LEFT(v)] + tree_[RIGHT(v)];
    }
}

