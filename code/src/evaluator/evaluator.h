#pragma once

#include "../graph/bipartite_graph/bip_graph.hpp"
#include "../solvers/solver_result.hpp"

long long evaluate(const bip_graph& g, const solver_result& solution);