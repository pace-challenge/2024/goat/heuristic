#include <algorithm>
#include "evaluator.h"
#include "segtree.hpp"

long long evaluate(const bip_graph& g, const solver_result& solution)
{
    std::vector<edge> all_edges;
    for (auto&& [u, adj]: g.fixed())
        for (auto&& v: adj)
            all_edges.push_back(edge {u, v});
    auto solperm = permutation(solution.order);
    const auto& fixedperm = g.get_fixed_permutation();
    std::sort(all_edges.begin(), all_edges.end(), [&](const edge& e, const edge& f)
    {
        return std::tuple(fixedperm.pos(e.u), solperm.pos(e.v)) <
               std::tuple(fixedperm.pos(f.u), solperm.pos(f.v));
    });
    
    seg_tree t(g.free().size());
    long long result = 0;
    for (auto& [u, v]: all_edges) {
        t.increment(solperm.pos(v));
        result += t.sum(solperm.pos(v) + 1, t.size() - 1);
    }
    return result;
    
}