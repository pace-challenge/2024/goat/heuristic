#pragma once

#include <vector>
#include <cstddef>

struct seg_tree
{
    /**
     * Constructs segment tree covering array of size n,
     * initializing everything to 0
     * @param n size of the underlying array
     */
    explicit seg_tree(size_t n);
    
    /**
     * Sums the interval [l,r]
     */
    long long sum(size_t l, size_t r);
    
    /**
     * Increments the value on given index i
     */
    void increment(size_t i);
    
    [[nodiscard]] size_t size() const;
  
  private:
    long long _sum(size_t lq, size_t rq, size_t ln, size_t rn, size_t v);
    
    void _increment(size_t i, size_t ln, size_t rn, size_t v);
    
    size_t n_;
    std::vector<long long> tree_;
};