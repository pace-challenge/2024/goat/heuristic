#pragma once

#include <vector>
#include <unordered_map>
#include <set>

#include "utilities.hpp"

template <typename T>
struct permutation
{
  public:
    explicit permutation(const std::vector<T>& order) : order_(order),
                                                        pos_(to_pos(order)) { }
    
    template <typename O> requires (!std::is_same_v<O, size_t>)
    explicit permutation(const std::unordered_map<T, O>& pos) : order_(to_ord(pos)),
                                                                pos_(to_pos(order_)) { }
    
    explicit permutation(const std::unordered_map<T, size_t>& pos) : order_(to_ord(pos)),
                                                                     pos_(pos) { }
    
    template <typename Cont>
    bool is_permutation_of(const Cont& items) const
    {
        return is_same_up_to_permutation(order_, items);
    }
    
    std::strong_ordering operator()(const T& u, const T& v) const
    {
        return pos(u) <=> pos(v);
    }
    
    size_t pos(const T& x) const
    {
        assert(pos_.contains(x));
        return pos_.at(x);
    }
    
    const T& operator[](size_t idx) const
    {
        return order_[idx];
    }
    
    T& operator[](size_t idx)
    {
        return order_[idx];
    }
    
    auto begin()
    {
        return order_.begin();
    }
    
    auto end()
    {
        return order_.end();
    }
    
    auto begin() const
    {
        return order_.begin();
    }
    
    auto end() const
    {
        return order_.end();
    }
    
    [[nodiscard]] size_t size() const
    {
        return order_.size();
    }
  
  private:
    std::vector<T> order_;
    std::unordered_map<T, size_t> pos_;
};