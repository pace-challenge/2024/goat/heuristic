#pragma once

#include <optional>
#include <map>
#include <unordered_set>
#include "../common.hpp"
#include "interval_info.hpp"

/**
 * For any two u,v \in FreePart(G) a pair {u,v} (u != v) is *forced* to u->v
 * if c(u,v) = 0 and c(v,u) > 0. A pair {u,v} is forced iff it is forced to u->v or v->u
 *
 * A pair {u,v} (u != v) is *orientable* if c(u,v) > 0, c(v,u) > 0
 *
 * A pair {u,v} is *free* iff c(u,v) = c(v,u) = 0
 */
struct costs
{
    static costs compute(bip_graph graph, interval_info intervals);
    costs(bip_graph graph,
          interval_info intervals,
          std::map<std::pair<vertex_t, vertex_t>, long long> c);
    
    /**
     * Returns c(u,v) if {u,v} is orientable, if u,v is free or forced, returns -1
     */
    long long get_cost(vertex_t u, vertex_t v) const;
    /**
     * Checks if c(u,v) == 0
     */
    long long is_c_zero(vertex_t u, vertex_t v) const;
    /**
     * Return c(u,v) for any u,v, if u,v is orientable, uses get_cost
     * if (u,v) is free check the interval info, otherwise bruteforce compute it
     */
    long long cost_at_any_cost(vertex_t u, vertex_t v) const;
    /**
     * Returns true iff c(u,v) = 0 and c(v,u) > 0
     */
    bool is_forced(vertex_t u, vertex_t v) const;
    /**
     * Returns true iff c(u,v) = c(v,u) = 0
     */
    bool is_free(vertex_t u, vertex_t v) const;
    /**
     * Returns true iff c(u,v) > 0 and c(v,u) > 0
     */
    bool is_orientable(vertex_t u, vertex_t v) const;
    
    bip_graph graph;
    interval_info intervals;
    std::map<std::pair<vertex_t, vertex_t>, long long> c;
};
