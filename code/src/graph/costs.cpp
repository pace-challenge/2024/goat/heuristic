#include "costs.hpp"

long long costs::get_cost(vertex_t u, vertex_t v) const
{
    if (c.contains({u, v}))
        return c.at({u, v});
    return -1;
}

costs::costs(bip_graph _graph,
             interval_info _intervals,
             std::map<std::pair<vertex_t, vertex_t>, long long int> _c) : graph(std::move(_graph)),
                                                                         intervals(std::move(_intervals)),
                                                                         c(std::move(_c)) { }

costs costs::compute(bip_graph graph, interval_info intervals)
{
    std::map<std::pair<vertex_t, vertex_t>, long long> c;
    
    std::unordered_set<vertex_t> Yx; //Yx, but in fact l_v < x <= r_v, not < (as stated in article)
    std::unordered_map<vertex_t, int> dlessxy; //d^{<x}(y)
    std::unordered_map<vertex_t, int> dlesseqxy; //d^{<=x}(y)
    for (auto&& x: graph.get_fixed_permutation()) {
        for (auto&& v: graph.fixed().at(x))
            dlesseqxy[v]++;
        
        for (auto&& y: intervals.x_left_endpoints.at(x))
            Yx.insert(y);
        for (auto&& u: graph.fixed().at(x))
            for (auto&& v: Yx)
                if (!intervals.ignore(u, v, graph)) {
                    c[{u, v}] += dlessxy[v];
                }
        
        for (auto&& u: Yx)
            for (auto&& v: graph.fixed().at(x))
                if (intervals.y_intervals.at(v).r == x && !intervals.ignore(u, v, graph)) {
                    c[{u, v}] +=
                            (long long) graph.free().at(v).size() *
                            ((long long) graph.free().at(u).size() - dlesseqxy[u]);
                }
        
        for (auto&& y: intervals.x_right_endpoints.at(x))
            Yx.erase(y);
        for (auto&& v: graph.fixed().at(x))
            dlessxy[v]++;
    }
    
    return costs(std::move(graph), std::move(intervals), std::move(c));
}

long long costs::is_c_zero(vertex_t u, vertex_t v)const
{
    if(graph.free().at(u).empty() || graph.free().at(v).empty())
        return true;
    auto& X_perm = graph.get_fixed_permutation();
    return X_perm(intervals.y_intervals.at(u).r, intervals.y_intervals.at(v).l) <= 0;
}

long long costs::cost_at_any_cost(vertex_t u, vertex_t v)const
{
    if (auto cost = get_cost(u, v); cost != -1)
        return cost;
    if (is_c_zero(u, v))
        return 0;
    //bruteforce it
    return graph.num_crossings(u, v);
}

bool costs::is_forced(vertex_t u, vertex_t v)const  {
    return is_c_zero(u,v) && !is_c_zero(v,u);
}

bool costs::is_free(vertex_t u, vertex_t v) const {
    return is_c_zero(u,v) && is_c_zero(v,u);
}

bool costs::is_orientable(vertex_t u, vertex_t v) const {
    return !is_c_zero(u,v) && !is_c_zero(v,u);
}
