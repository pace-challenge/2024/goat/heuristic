#include "../common.hpp"
#include <vector>
#include <unordered_map>
#include <unordered_set>


/*
* Class representing transitive DAG (i.e. if u->v and v->w is in the graph then u->w is also in it)
*/
struct DAG
{
    
    /*
    * Adds @u->@v edge to DAG and also adds all transitive edges.
    * returns all added edges including u->v
    */
    std::vector<std::pair<vertex_t, vertex_t>> commit_edge(vertex_t u, vertex_t v);
    
    /*
    * Returns all transitive edges if u->v would be added
    */
    std::vector<std::pair<vertex_t, vertex_t>> get_transitive_edges_if_add(vertex_t u, vertex_t v) const;
    
    /*
    * Returns true if u->v, otherwise false
    */
    bool precedes(vertex_t u, vertex_t v) const;
    
    /*
    * Returns true if u->v or v->u, otherwise false
    */
    bool comparable(vertex_t u, vertex_t v) const;
    
    /*
    * Initializes empty dag with @vertices as vertices
    */
    explicit DAG(const std::vector<vertex_t>& vertices);
  
  private:
    /*
    * Builds the dag from the provided edges.  
    * returns only the transitive edges
    */
    std::vector<std::pair<vertex_t, vertex_t>> build(const std::vector<std::pair<vertex_t, vertex_t>> & edges);

    private:
    /*
    * Adds @u->@v edge without checking transitive edges
    * The edge or its reverse can't be in the graph
    */
    void add_edge(vertex_t u, vertex_t v);
    
    std::unordered_map<vertex_t, std::unordered_set<vertex_t>> pred_;
    std::unordered_map<vertex_t, std::unordered_set<vertex_t>> succ_;
};