#include "interval_info.hpp"

bool interval::operator<(const interval& o) const
{
    return std::tie(l, r) < std::tie(o.l, o.r);
}

bool interval_info::ignore(vertex_t u, vertex_t v, const bip_graph& g) const
{
    if (u == v)
        return true;
    auto ru = y_intervals.at(u).r;
    auto lu = y_intervals.at(u).l;
    auto rv = y_intervals.at(v).r;
    auto lv = y_intervals.at(v).l;
    auto& X_perm = g.get_fixed_permutation();
    return X_perm(ru, lv) <= 0 || X_perm(rv, lu) <= 0;
}

interval_info interval_info::compute(const bip_graph& graph)
{
    interval_info ret;
    auto& X_perm = graph.get_fixed_permutation();
    //initialize with empty sets
    for (auto& x: X_perm) {
        ret.x_left_endpoints[x] = {};
        ret.x_right_endpoints[x] = {};
    }
    for (auto&& [y, Ny]: graph.free()) {
        if (Ny.empty()) {
            ret.y_intervals[y] = {.l=INVALID_VERTEX, .r=INVALID_VERTEX};
            continue;
        }
        interval i = {.l = *Ny.begin(), .r = *Ny.begin()};
        for (auto& x: Ny) {
            if (X_perm(x, i.l) < 0)
                i.l = x;
            if (X_perm(i.r, x) < 0)
                i.r = x;
        }
        ret.y_intervals[y] = i;
        ret.x_left_endpoints[i.l].insert(y);
        ret.x_right_endpoints[i.r].insert(y);
    }
    return ret;
}