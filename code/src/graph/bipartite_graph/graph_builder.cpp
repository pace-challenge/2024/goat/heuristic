#include "graph_builder.hpp"

void graph_builder::set_fixed_part(std::vector<vertex_t>&& part)
{
    fixed_part_ = std::move(part);
    fixed_part_set_ = std::set(fixed_part_.begin(), fixed_part_.end());
}

void graph_builder::set_free_part(std::vector<vertex_t>&& part)
{
    free_part_ = std::move(part);
    free_part_set_ = std::set(free_part_.begin(), free_part_.end());
}

bool graph_builder::add_edge(vertex_t x, vertex_t y)
{
    THROW_IF_NOT(fixed_part_set_.contains(x) && free_part_set_.contains(y), "Invalid edge to add");
    return edges_.insert({x, y}).second;
}

bip_graph graph_builder::build() const
{
    std::unordered_map<vertex_t, std::unordered_set<vertex_t>> X;
    std::unordered_map<vertex_t, std::unordered_set<vertex_t>> Y;
    for (auto& x: fixed_part_) X[x];
    for (auto& y: free_part_) Y[y];
    for (auto& [x, y]: edges_)
        X[x].insert(y), Y[y].insert(x);
    return bip_graph(permutation(fixed_part_), X, Y);
}

void graph_builder::reset()
{
    fixed_part_.clear();
    fixed_part_set_.clear();
    free_part_.clear();
    free_part_set_.clear();
    edges_.clear();
}

bool graph_builder::remove_edge(vertex_t x, vertex_t y)
{
    THROW_IF_NOT(fixed_part_set_.contains(x) && free_part_set_.contains(y), "Invalid edge to remove");
    return edges_.erase({x, y}) == 1;
}

void graph_builder::add_neiy(vertex_t y, const std::vector<vertex_t>& nb)
{
    for (auto& v: nb)
        add_edge(v, y);
}

void graph_builder::add_neix(vertex_t x, const std::vector<vertex_t>& nb)
{
    for (auto& v: nb)
        add_edge(x, v);
}

