#include "bip_graph.hpp"

bip_graph::bip_graph(const permutation<vertex_t>& fixed_permutation,
                     std::unordered_map<vertex_t, std::unordered_set<vertex_t>>& fixed_part,
                     std::unordered_map<vertex_t, std::unordered_set<vertex_t>>& free_part) : fixed_part_permutation_(
        fixed_permutation),
                                                                                              fixed_part_(fixed_part),
                                                                                              free_part_(free_part)
{
    validate_structure();
}

long long bip_graph::number_of_crossings(const permutation<vertex_t>& perm) const
{
    assert(perm.size() == free_part_.size());
    assert(perm.is_permutation_of(get_free_part_as<std::vector>()));
    long long cnt = 0;
    for (size_t i = 0; i < perm.size(); ++i)
        for (size_t j = i + 1; j < perm.size(); ++j)
            cnt += num_crossings(perm[i], perm[j]);
    return cnt;
}

void bip_graph::validate_structure() const
{
    using namespace std::string_literals;
    for (auto& [x, adj]: fixed_part_)
        for (auto& y: adj)
            throw_if_not(free_part_.contains(y),
                         std::to_string(x) + " is in fixed and is adjacent to " + std::to_string(y) + " but " +
                         std::to_string(y) + " is not in free");
    for (auto& [y, adj]: free_part_)
        for (auto& x: adj)
            throw_if_not(fixed_part_.contains(x),
                         std::to_string(y) + " is in free and is adjacent to " + std::to_string(x) + " but " +
                         std::to_string(x) + " is not in fixed");
    throw_if_not(fixed_part_permutation_.is_permutation_of(get_fixed_part_as<std::vector>()), "X_permutation is invalid");
}

long long bip_graph::num_crossings(vertex_t u, vertex_t v) const
{
    assert(free_part_.contains(u));
    assert(free_part_.contains(v));
    long long cnt = 0;
    for (auto& x: free_part_.at(u)) {
        assert(fixed_part_.contains(x));
        for (auto& y: free_part_.at(v)) {
            assert(fixed_part_.contains(y));
            if (fixed_part_permutation_(y, x) < 0)
                cnt++;
        }
    }
    return cnt;
}

const std::unordered_map<vertex_t, std::unordered_set<vertex_t>>& bip_graph::free() const
{
    return free_part_;
}

const std::unordered_map<vertex_t, std::unordered_set<vertex_t>>& bip_graph::fixed() const
{
    return fixed_part_;
}

const permutation<vertex_t>& bip_graph::get_fixed_permutation() const
{
    return fixed_part_permutation_;
}