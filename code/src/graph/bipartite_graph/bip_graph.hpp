#pragma once

#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <string>
#include "../../utilities.hpp"
#include "../../permutation.hpp"
#include "../../common.hpp"


struct bip_graph
{
    bip_graph(const permutation<vertex_t>& fixed_permutation,
              std::unordered_map<vertex_t, std::unordered_set<vertex_t>>& fixed_part,
              std::unordered_map<vertex_t, std::unordered_set<vertex_t>>& free_part);
    
    /**
     * @brief Computes the number of num_crossings when the free part is permuted like perm
     */
    long long number_of_crossings(const permutation<vertex_t>& perm) const;
    
    /**
     * Validates the structure of this bipartite graph.
     * Throws if the graph is invalid.
     */
    void validate_structure() const;
    
    /**
     * @brief Computes the number of crossing when u is before v in the ordering
     * @param u, v - the two vertices from the free part
     */
    long long num_crossings(vertex_t u, vertex_t v) const;
    
    /**
     * Gets the fixed partition as list of neighbors for each vertex.
     */
    const std::unordered_map<vertex_t, std::unordered_set<vertex_t>>& fixed() const;
    
    /**
     * Gets the free partition as list of neighbors for each vertex.
     */
    const std::unordered_map<vertex_t, std::unordered_set<vertex_t>>& free() const;
    
    template <template <typename...> typename Container, typename ... CArgs>
    auto get_free_part_as() const
    {
        Container<vertex_t, CArgs...> c;
        std::transform(free_part_.begin(), free_part_.end(),
                       std::inserter(c, c.end()),
                       [](auto& kv) { return kv.first; });
        return c;
    }
    
    template <template <typename...> typename Container, typename ... CArgs>
    auto get_fixed_part_as() const
    {
        Container<vertex_t, CArgs...> c;
        std::transform(fixed_part_.begin(), fixed_part_.end(),
                       std::inserter(c, c.end()),
                       [](auto& kv) { return kv.first; });
        return c;
    }
    
    const permutation<vertex_t>& get_fixed_permutation() const;
  
  private:
    permutation<vertex_t> fixed_part_permutation_;
    std::unordered_map<vertex_t, std::unordered_set<vertex_t>> fixed_part_;
    std::unordered_map<vertex_t, std::unordered_set<vertex_t>> free_part_;
};



