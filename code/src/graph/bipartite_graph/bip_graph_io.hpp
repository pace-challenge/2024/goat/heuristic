#pragma once

#include <iostream>
#include "bip_graph.hpp"

/**
 * Reads graph from given input stream given in the PACE format
 * @see https://pacechallenge.org/2024/io/
 * @return constructed graph
 */
bip_graph load_bip_graph_pace(std::istream& input);

/**
 * Outputs graph in the PACE2024 output format
 * @see https://pacechallenge.org/2024/io/
 * Function performs validation check and throw std::invalid_argument if the
 * graph is not in pace format
 */
void output_bip_graph_pace(const bip_graph& g, std::ostream& output, const std::string& comment = "");