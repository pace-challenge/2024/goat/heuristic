#include <numeric>
#include <sstream>
#include <functional>
#include "bip_graph_io.hpp"
#include "graph_builder.hpp"

template <typename T>
std::vector<T> iota_vec(size_t n, T val = T())
{
    std::vector<T> ret(n);
    std::iota(ret.begin(), ret.end(), val);
    return ret;
}

/**
 * Returns next line from the input, skips empty lines
 * and lines starting with 'c' (indicating a comment)
 * @see https://pacechallenge.org/2024/io/
 * @return stringstream with the given line
 */
std::stringstream retrieve_line(std::istream& input)
{
    std::string line;
    while (true) {
        std::getline(input, line);
        if (line.empty() || line[0] == 'c')
            continue;
        return std::stringstream(std::move(line));
    }
}

bip_graph load_bip_graph_pace(std::istream& input)
{
    auto previous_exceptions = input.exceptions();
    input.exceptions(std::ios::failbit);
    
    std::string p, ocr;
    size_t x_size, y_size, edge_count;
    retrieve_line(input) >> p >> ocr >> x_size >> y_size >> edge_count;
    THROW_IF_NOT(p == "p", "expected p");
    THROW_IF_NOT(ocr == "ocr", "expected ocr");
    
    graph_builder builder;
    builder.set_fixed_part(iota_vec<vertex_t>(x_size, 1));
    builder.set_free_part(iota_vec<vertex_t>(y_size, (vertex_t) x_size + 1));
    
    for (size_t i = 0; i < edge_count; i++) {
        vertex_t x, y;
        retrieve_line(input) >> x >> y;
        THROW_IF_NOT(1 <= x && x <= x_size, "x must be between 1 and x_size, x_size = " + std::to_string(x_size));
        THROW_IF_NOT(x_size + 1 <= y && y <= x_size + y_size,
                     "y must be between x_size + 1 and x_size + y_size, x_size+1 = " + std::to_string(x_size + 1) +
                     ", x_size+y_size = " + std::to_string(x_size + y_size));
        THROW_IF_NOT(builder.add_edge(x, y),
                     "failed to add edge, probably it is duplicate? x = " + std::to_string(x) + ", y=" +
                     std::to_string(y));
    }
    
    input.exceptions(previous_exceptions);
    return builder.build();
}

/**
 * Checks that @str contains no newlines
 */
bool contains_no_newlines(const std::string& str)
{
    auto is_newline = [](char c) { return c == '\n'; };
    return std::all_of(str.begin(), str.end(), std::not_fn(is_newline));
}

bool is_pace_format(const bip_graph& g)
{
    auto A = g.fixed().size();
    auto B = g.free().size();
    for (auto& [x, _]: g.fixed())
        if (x < 1 || x > A)
            return false;
    for (auto& [y, _]: g.free())
        if (y < A + 1 || y > A + B)
            return false;
    return true;
}


void output_bip_graph_pace(const bip_graph& g, std::ostream& output, const std::string& comment)
{
    if (!comment.empty()) {
        if (!contains_no_newlines(comment))
            throw std::invalid_argument("Comment contains newlines.");
        output << "c " << comment << '\n';
    }
    if (!is_pace_format(g))
        throw std::invalid_argument("Graph is not in pace format.");
    std::vector<edge> all_edges;
    for (auto&& [u, adju]: g.fixed())
        for (auto&& v: adju)
            all_edges.push_back(edge {u, v});
    
    output << "p ocr " << g.fixed().size() << ' ' << g.free().size() << ' ' << all_edges.size() << '\n';
    for (auto&& [u, v]: all_edges)
        output << u << ' ' << v << '\n';
    output.flush();
}