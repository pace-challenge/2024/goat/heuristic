#include "digraph.hpp"

bool digraph::add_vertex(vertex_t v) {
    return (int) succ_.insert({v, {}}).second
           &
           (int) pred_.insert({v, {}}).second;
}

bool digraph::add_edge(vertex_t from, vertex_t to) {
    pred_[from];
    succ_[to];
    return (int) succ_[from].insert(to).second
           &
           (int) pred_[to].insert(from).second;
}

void digraph::remove_edge(vertex_t from, vertex_t to)
{
    succ_[from].erase(to);
    pred_[to].erase(from);
}
