#include "utilities.hpp"

std::optional<std::vector<vertex_t>> topsort(const digraph& G)
{
    //classical topsort
    std::unordered_map<vertex_t, std::size_t> deg;
    for (auto& [u, succ]: G.succ()) {
        deg[u]; //do not forget to include vertices with indegree 0 (deg is unordered map)
        for (auto& v: succ)
            ++deg[v];
    }
    const std::size_t n = deg.size();
    std::vector<vertex_t> q;
    for (auto& [v, d]: deg)
        if (d == 0)
            q.push_back(v);
    for (size_t i = 0; i < q.size(); ++i)
        for (auto& v: G.succ(q[i]))
            if (--deg[v] == 0)
                q.push_back(v);
    if (q.size() == n)
        return std::optional {q};
    else
        return std::nullopt;
}
