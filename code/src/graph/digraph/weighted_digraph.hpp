#pragma once

#include <map>
#include <stdexcept>
#include "digraph.hpp"


template <typename Weight = long long>
class weighted_digraph : public digraph
{
  public:
    const Weight& weight(vertex_t from, vertex_t to) const
    {
        THROW_IF_NOT(weights_.contains({from, to}), "invalid edge");
        return weights_.at({from, to});
    }
    
    bool add_weighted_edge(vertex_t from, vertex_t to, Weight w)
    {
        return digraph::add_edge(from, to) && ((weights_[{from, to}] = w), true);
    }
    
    int e() const
    {
        return weights_.size();
    }
    
    bool add_edge(vertex_t, vertex_t)
    {
        throw std::logic_error("Cannot add nonweighted edge to weighted graph.");
    }
    
    void remove_edge(vertex_t from, vertex_t to)
    {
        digraph::remove_edge(from, to);
        weights_.erase({from, to});
    }
  
  private:
    std::map<edge, Weight, decltype(edge_cmp_coordinate_wise)> weights_;
};