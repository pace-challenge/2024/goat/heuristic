#pragma once
#include <optional>
#include <vector>
#include "weighted_digraph.hpp"

/**
 * Returns topological order if G is acyclic, otherwise empty std::optional
 */
std::optional<std::vector<vertex_t>> topsort(const digraph& G);

inline bool is_acyclic(const digraph& G)
{
    return topsort(G) != std::nullopt;
}