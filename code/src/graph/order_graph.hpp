#pragma once
#include "costs.hpp"
#include "digraph/weighted_digraph.hpp"

/*
 * If c(u,v) > 0 and c(v,u) > 0, we want edge u->v if c(u,v) < c(v,u) and v->u otherwise
 *
 * For (u,v) :
 * if c(u,v) == c(v,u) then no edge.
 * if c(u,v) != c(v,u) then u -> v iff c(u,v) < c(v,u)
 *                          v -> u iff c(v,u) < c(u,v)
 *
 * */

digraph make_order_graph(const costs& c);


weighted_digraph<long long> make_cost_graph(const costs& c);