#pragma once

#include <cassert>
#include <iostream>
#include <string>

class Tester
{
  public:
    Tester() : checking(false) { }
    
    void check(const std::string& s)
    {
        if (checking)
            throw std::runtime_error("Cannot check two things at once");
        std::cout << "Testing " << s << " ";
        checking = true;
    }
    
    void ok()
    {
        if (!checking)
            throw std::runtime_error("Wasn't checking anything");
        std::cout << ' ' << "\033[32m" << "OK" << "\033[0m" << std::endl;
        checking = false;
    }

#define is_true(q) check_true(#q,(q))
    
    void check_true(const std::string& s, bool val) const
    {
        if (!checking)
            throw std::runtime_error("Must be checking something");
        if (!val)
        {
            std::cout << ' ';
            std::cout << "\033[31m";
            std::cout << "test '" << s << "' failed!" << std::endl;
            std::cout << "\033[0m";
            std::cout.flush();
            exit(1);
        }
        else
        {
            std::cout << '.';
            std::cout.flush();
        }
    }
  
  private:
    bool checking;
};
