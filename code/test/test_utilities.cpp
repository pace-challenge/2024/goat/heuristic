#include "tester.hpp"
#include <vector>
#include "../src/utilities.hpp"

int main()
{
	Tester t;
	t.check("is unque");
	{
		std::vector<int> a = {1, 2, 3, 4, 5};
		t.is_true(is_unique(a));
		std::vector<int> b = {1000, 5000, 1530213};
		t.is_true(is_unique(b));
		std::vector<int> c = {1, 5, 3, 2, 6};
		t.is_true(is_unique(c));
		std::vector<int> d = {1, 5, 1};
		t.is_true(!is_unique(d));
	}
	t.ok();
	
	t.check("to pos_");
	{
		std::vector<int> order = {1, 9, 4, 6, 5, 3};
		auto m = to_pos(order);
		for(auto& item: order)
			t.is_true(m.contains(item));
		for(size_t i = 0; i < order.size(); ++i)
			t.is_true(m[order[i]] == i);
	}
	t.ok();
	t.check("to ord");
	{
		std::unordered_map<std::string, size_t> pos = {
				{"a", 2},
				{"b", 4},
				{"c", 1},
				{"d", 3}
		};
		auto ord = to_ord(pos);
		t.is_true(ord == std::vector<std::string>({"c", "a", "d", "b"}));
	}
	t.ok();
    
    t.check("is_same_rotation");
    {
        std::vector<int> a = {1,2,3,4,5};
        std::vector<int> b = {3,4,5,1,2};
        t.is_true(is_rotation_of(a,b));
        
        std::vector<int> aa = {1,2,3,4,5};
        std::vector<int> bb = {1,2,4,3,5};
        t.is_true(!is_rotation_of(aa,bb));
    }
    t.ok();
}