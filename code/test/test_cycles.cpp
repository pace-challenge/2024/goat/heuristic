#include "tester.hpp"
#include "../src/graph/order_graph.hpp"
#include "../src/lower_bounds/costs_lb_improved.hpp"

int main()
{
    Tester t;
    t.check("basic");
    {
        weighted_digraph<long long> G;
        G.add_vertices(1,2,3);
        G.add_weighted_edge(1,2,3);
        G.add_weighted_edge(2,3,2);
        G.add_weighted_edge(3,1,3);
        std::vector<vertex_t> cycle;
        t.is_true(!exists_cycle(G,cycle,3));
        t.is_true(cycle.empty());
        t.is_true(exists_cycle(G,cycle,2));
        t.is_true(is_rotation_of(cycle,std::vector<vertex_t>{1,2,3}));
        t.is_true(exists_cycle(G,cycle,1));
        t.is_true(is_rotation_of(cycle,std::vector<vertex_t>{1,2,3}));
        t.is_true(exists_cycle(G,cycle,0));
        t.is_true(is_rotation_of(cycle,std::vector<vertex_t>{1,2,3}));
        t.is_true(!exists_cycle(G,cycle,4));
        t.is_true(cycle.empty());
        t.is_true(find_cycle(G,cycle));
        t.is_true(is_rotation_of(cycle,std::vector<vertex_t>{1,2,3}));
        t.ok();
    }
    t.check("acyclic");
    {
        weighted_digraph<long long> G;
        G.add_vertices(1,2,3,4);
        G.add_weighted_edge(4,2,10);
        G.add_weighted_edge(4,3,10);
        G.add_weighted_edge(2,1,10);
        G.add_weighted_edge(3,1,10);
        std::vector<vertex_t> cycle;
        for(int i = 0; i < 10; ++i)
            t.is_true(!exists_cycle(G,cycle,3));
        t.ok();
    }
}