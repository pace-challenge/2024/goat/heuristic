#include "tester.hpp"
#include "../src/solvers/exact/bf_solver.hpp"
#include "../src/graph/bipartite_graph/graph_builder.hpp"


int main()
{
    
    Tester t;
    t.check("basic graph");
    {
        graph_builder b;
        b.set_fixed_part({1, 2, 3});
        b.set_free_part({4, 5, 6});
        b.add_edge(1, 4);
        b.add_edge(2, 6);
        b.add_edge(3, 5);
        auto g = b.build();
        auto res = brute_force_solve(g);
        t.is_true(g.number_of_crossings(permutation(res.order)) == 0);
        t.is_true(res.is_valid_result_of(g));
    }
    {
        //K33
        graph_builder b;
        b.set_fixed_part({1, 2, 3});
        b.set_free_part({4, 5, 6});
        for (auto& x: {1, 2, 3})
            for (auto& y: {4, 5, 6})
                b.add_edge(x, y);
        auto g = b.build();
        auto res = brute_force_solve(g);
        t.is_true(g.number_of_crossings(permutation(res.order)) == 9);
        t.is_true(res.is_valid_result_of(g));
    }
    {
        //K33 without an edge
        graph_builder b;
        b.set_fixed_part({1, 2, 3});
        b.set_free_part({4, 5, 6});
        for (auto& x: {1, 2, 3})
            for (auto& y: {4, 5, 6})
                if (!(x == 1 && y == 4))
                    b.add_edge(x, y);
        auto g = b.build();
        auto res = brute_force_solve(g);
        t.is_true(g.number_of_crossings(permutation(res.order)) == 5);
        t.is_true(res.is_valid_result_of(g));
    }
    
    t.ok();
}