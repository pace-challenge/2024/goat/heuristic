#include "tester.hpp"
#include "../src/generator/generator.hpp"
#include "../src/evaluator/evaluator.h"
#include <numeric>
#include <algorithm>
#include <random>

int main()
{
    Tester t;
    t.check("random small graphs");
    {
        bip_graph_generator gen(42);
        for (size_t i = 1; i < 50; ++i) {
            auto g = gen.gen_graph(5 + 4 * i, 7 + 5 * i, 25 + 10 * i);
            auto free = g.get_free_part_as<std::vector>();
            std::shuffle(free.begin(), free.end(), gen.get_rng());
            auto fasteval = evaluate(g, solver_result(free));
            auto sloweval = g.number_of_crossings(permutation(free));
            t.is_true(fasteval == sloweval);
        }
        t.ok();
    }
}