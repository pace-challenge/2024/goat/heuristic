#include "tester.hpp"

#include "../src/graph/bipartite_graph/graph_builder.hpp"
#include "../src/lower_bounds/costs_lb.hpp"
#include <numeric>
#include <algorithm>
#include <random>
#include "../src/generator/generator.hpp"

int main()
{
    Tester t;
    t.check("small random");
    {
        bip_graph_generator gen(42);
        for (size_t iter = 0; iter < 2500; ++iter) {
            auto g = gen.gen_graph(6, 6, 15);
            auto lbnaive = costs_lower_bound_naive(g);
            auto lbbetter = costs_lower_bound(g);
            auto c = costs::compute(g, interval_info::compute(g));
            t.is_true(lbnaive == lbbetter);
        }
        t.ok();
    }
}