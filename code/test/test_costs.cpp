#include "tester.hpp"

#include "../src/graph/bipartite_graph/graph_builder.hpp"
#include "../src/generator/generator.hpp"
#include "../src/graph/costs.hpp"
#include <numeric>
#include <random>

int main()
{
    Tester t;
    t.check("small");
    {
        graph_builder gb;
        gb.set_fixed_part({'a', 'b', 'c', 'd'});
        gb.set_free_part({1, 2});
        gb.add_neiy(1, {'a', 'b', 'c', 'd'});
        gb.add_neiy(2, {'a', 'b'});
        
        auto g = gb.build();
        auto c = costs::compute(g, interval_info::compute(g));
        t.is_true(c.get_cost(1, 2) == 5);
        t.is_true(c.get_cost(2, 1) == 1);
        t.ok();
    }
    t.check("bigger");
    {
        graph_builder gb;
        
        gb.set_fixed_part({'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'});
        gb.set_free_part({1, 2, 3, 4, 5, 6, 7, 8});
        gb.add_neiy(1, {'a', 'b'});
        gb.add_neiy(2, {'a', 'b', 'c', 'd'});
        gb.add_neiy(3, {'b', 'd'});
        gb.add_neiy(4, {'a', 'c', 'f'});
        gb.add_neiy(5, {'b', 'c', 'd', 'e'});
        gb.add_neiy(6, {'d', 'e', 'g'});
        gb.add_neiy(7, {'e', 'f', 'h'});
        gb.add_neiy(8, {'g', 'h'});
        
        auto g = gb.build();
        auto c = costs::compute(g, interval_info::compute(g));
        for (auto& u: g.get_free_part_as<std::vector>())
            for (auto& v: g.get_free_part_as<std::vector>()) {
                if (u == v)
                    continue;
                auto uv = g.num_crossings(u, v);
                auto vu = g.num_crossings(v, u);
                auto vuc = c.get_cost(v, u);
                auto uvc = c.get_cost(u, v);
                if (vu != 0 && uv != 0) {
                    t.is_true(uvc == uv);
                    t.is_true(vuc == vu);
                } else {
                    t.is_true(uvc == -1);
                    t.is_true(vuc == -1);
                }
                
            }
        t.ok();
    }
    t.check("random");
    {
        bip_graph_generator gen(42);
        auto g = gen.gen_graph(250,250,2500);
        auto c = costs::compute(g, interval_info::compute(g));
        for (auto& u: g.get_free_part_as<std::vector>())
            for (auto& v: g.get_free_part_as<std::vector>()) {
                if (u == v)
                    continue;
                auto uv = g.num_crossings(u, v);
                auto vu = g.num_crossings(v, u);
                auto vuc = c.get_cost(v, u);
                auto uvc = c.get_cost(u, v);
                if (vu != 00 && uv != 0) {
                    t.is_true(uvc == uv);
                    t.is_true(vuc == vu);
                } else {
                    t.is_true(uvc == -1);
                    t.is_true(vuc == -1);
                }
            }
        t.ok();
    }
    t.check("edge case");
    {
        
        bip_graph_generator gen(42);
        for (size_t iter = 0; iter < 2500; ++iter) {
            auto g = gen.gen_graph(6,6,15);
            auto c = costs::compute(g,interval_info::compute(g));
            for (auto& u: g.get_free_part_as<std::vector>())
                for (auto& v: g.get_free_part_as<std::vector>()) {
                    if (u == v)
                        continue;
                    auto uv = g.num_crossings(u, v);
                    auto vu = g.num_crossings(v, u);
                    auto vuc = c.get_cost(v, u);
                    auto uvc = c.get_cost(u, v);
                    if (vu != 0 && uv != 0) {
                        t.is_true(uvc == uv);
                        t.is_true(vuc == vu);
                    } else {
                        t.is_true(uvc == -1);
                        t.is_true(vuc == -1);
                    }
                }
        }
        
        t.ok();
    }
}