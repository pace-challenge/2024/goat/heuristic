#include "tester.hpp"

#include "../src/graph/bipartite_graph/graph_builder.hpp"
#include "../src/reduction_rules/RR2.hpp"

int main()
{
    Tester t;
    t.check("simple");
    {
        graph_builder gb;
        gb.set_fixed_part({'a', 'b', 'c', 'd'});
        gb.set_free_part({1, 2, 3});
        gb.add_neiy(1, {'a', 'b', 'd'});
        gb.add_neiy(2, {'a', 'c'});
        gb.add_neiy(3, {'a', 'b', 'd'});

        auto g = gb.build();
        auto c = costs::compute(g,interval_info::compute(g));
        auto c_1_2 = c.get_cost(1,2);
        auto c_2_1 = c.get_cost(2,1);

        t.is_true(c.get_cost(1, 3) == 3);
        t.is_true(c.get_cost(3, 1) == 3);
        RR2(g, c);
        t.is_true(c.get_cost(1, 2) == c_1_2 * 2);
        t.is_true(c.get_cost(1, 3) == -1);
        t.is_true(c.get_cost(2, 1) == c_2_1 * 2);
        t.is_true(c.get_cost(2, 3) == -1);
        t.is_true(c.get_cost(3, 1) == -1);
        t.is_true(c.get_cost(3, 2) == -1);

        t.ok();
    }
}