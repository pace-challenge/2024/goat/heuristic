#include <sstream>
#include "../src/graph/bipartite_graph/bip_graph_io.hpp"
#include "tester.hpp"
#include "../src/generator/generator.hpp"

int main()
{
    
    Tester t;
    t.check("Loading from stream, no comments");
    {
        std::string s =
                "p ocr 10 10 41\n"
                "1 11\n"
                "3 11\n"
                "4 11\n"
                "5 11\n"
                "6 11\n"
                "9 11\n"
                "10 11\n"
                "3 12\n"
                "4 12\n"
                "10 12\n"
                "6 13\n"
                "9 13\n"
                "10 13\n"
                "1 14\n"
                "10 14\n"
                "1 15\n"
                "4 15\n"
                "5 15\n"
                "8 15\n"
                "10 15\n"
                "8 16\n"
                "9 16\n"
                "1 17\n"
                "2 17\n"
                "6 17\n"
                "8 17\n"
                "10 17\n"
                "2 18\n"
                "4 18\n"
                "5 18\n"
                "6 18\n"
                "7 18\n"
                "2 19\n"
                "3 19\n"
                "4 19\n"
                "5 19\n"
                "8 19\n"
                "10 19\n"
                "2 20\n"
                "7 20\n"
                "10 20\n";;
        std::stringstream ss(s);
        auto g = load_bip_graph_pace(ss);
        t.is_true(is_same_up_to_permutation(g.get_fixed_part_as<std::vector>(), std::vector {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}));
        t.is_true(is_same_up_to_permutation(g.get_free_part_as<std::vector>(), std::vector {11, 12, 13, 14, 15, 16, 17, 18, 19, 20}));
        ss.str(s);
        size_t xs, ys, m;
        std::string p, ocr;
        ss >> p >> ocr >> xs >> ys >> m;
        auto&& X = g.fixed();
        auto&& Y = g.free();
        while (m--) {
            vertex_t x, y;
            ss >> x >> y;
            t.is_true(X.contains(x) && X.at(x).contains(y));
            t.is_true(Y.contains(y) && Y.at(y).contains(x));
        }
    }
    t.ok();
    
    t.check("loading from stream with comments");
    {
        std::string s =
                "c commentXYZ\n"
                "p ocr 10 10 41\n"
                "c comment haha\n"
                "1 11\n"
                "3 11\n"
                "c this is a comment\n"
                "4 11\n"
                "5 11\n"
                "6 11\n"
                "c this is a nice comment\n"
                "9 11\n"
                "10 11\n"
                "3 12\n"
                "4 12\n"
                "10 12\n"
                "6 13\n"
                "9 13\n"
                "10 13\n"
                "1 14\n"
                "10 14\n"
                "1 15\n"
                "4 15\n"
                "c this edge is important!\n"
                "5 15\n"
                "8 15\n"
                "10 15\n"
                "8 16\n"
                "9 16\n"
                "1 17\n"
                "2 17\n"
                "6 17\n"
                "8 17\n"
                "10 17\n"
                "2 18\n"
                "4 18\n"
                "5 18\n"
                "6 18\n"
                "7 18\n"
                "2 19\n"
                "3 19\n"
                "4 19\n"
                "5 19\n"
                "8 19\n"
                "10 19\n"
                "2 20\n"
                "7 20\n"
                "10 20\n";;
        std::stringstream ss(s);
        auto g = load_bip_graph_pace(ss);
        t.is_true(is_same_up_to_permutation(g.get_fixed_part_as<std::vector>(), std::vector {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}));
        t.is_true(is_same_up_to_permutation(g.get_free_part_as<std::vector>(), std::vector {11, 12, 13, 14, 15, 16, 17, 18, 19, 20}));
        t.ok();
    }
    
    auto get_all_edges = [](const bip_graph& g)
    {
        std::vector<edge> ret;
        for (auto&& [x, adjx]: g.fixed())
            for (auto&& y: adjx)
                ret.push_back(edge {x, y});
        sort(ret.begin(), ret.end(), edge_cmp_coordinate_wise);
        return ret;
    };
    
    t.check("read and write");
    {
        bip_graph_generator gen(42);
        auto g = gen.gen_graph(15, 15, 100);
        std::stringstream ss;
        output_bip_graph_pace(g, ss);
        auto h = load_bip_graph_pace(ss);
        t.is_true(get_all_edges(g) == get_all_edges(h));
        t.ok();
    }
}
