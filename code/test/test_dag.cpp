#include "../src/graph/dag.hpp"
#include "tester.hpp"

using namespace std;

int main()
{
	Tester t;
	t.check("path");
	{
		DAG d({1,2,3,4});
        t.is_true(!d.comparable(1,2));
        d.commit_edge(1,2);
        t.is_true(d.comparable(1,2));
        t.is_true(d.precedes(1,2));
        t.is_true(!d.precedes(3,4));
        d.commit_edge(3,4);
        t.is_true(d.precedes(3,4));
        t.is_true(!d.precedes(2,3));
        d.commit_edge(2,3);
        for(int i = 1; i < 5; ++i){
            for(int j = 1; j < 5; ++j){
                if(i == j){
                    continue;
                }
                t.is_true(d.precedes(i,j) == (i < j));
                t.is_true(d.comparable(i,j));
            }
        }
	}
	t.ok();
}