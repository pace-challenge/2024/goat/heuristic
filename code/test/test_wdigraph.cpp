#include "tester.hpp"
#include <vector>
#include "../src/utilities.hpp"
#include "../src/graph/digraph/weighted_digraph.hpp"
#include "../src/graph/digraph/utilities.hpp"

int main()
{
    Tester t;
    t.check("small graphs");
    {
        {
            digraph G;
            G.add_edge(0, 1);
            G.add_edge(1, 0);
            t.is_true(!is_acyclic(G));
        }
        {
            digraph G;
            G.add_edge(0, 1);
            t.is_true(is_acyclic(G));
        }
        {
            digraph G;
            G.add_vertex(2);
            G.add_edge(0, 1);
            t.is_true(is_acyclic(G));
        }
        {
            digraph G;
            t.is_true(is_acyclic(G));
        }
        {
            digraph G;
            G.add_edge(0, 1);
            G.add_edge(1, 2);
            G.add_edge(2, 0);
            t.is_true(!is_acyclic(G));
        }
        {
            digraph G;
            G.add_edge(0, 1);
            G.add_edge(1, 2);
            G.add_edge(2, 0);
            G.add_edge(3, 0);
            t.is_true(!is_acyclic(G));
        }
        {
            digraph G;
            G.add_edge(0, 1);
            G.add_edge(1, 2);
            G.add_edge(2, 0);
            G.add_edge(2, 3);
            G.add_edge(3, 4);
            G.add_edge(4, 2);
            t.is_true(!is_acyclic(G));
        }
        {
            digraph G;
            G.add_edge(0, 1);
            G.add_edge(1, 2);
            auto o = topsort(G);
            t.is_true((bool) o);
            t.is_true((*o == std::vector{0_v, 1_v, 2_v}));
        }
        t.ok();
    }
}