#include "../src/graph/bipartite_graph/bip_graph.hpp"
#include "tester.hpp"


int main()
{
	Tester t;
	t.check("small graphs");
	{
		std::vector<vertex_t> X = {1, 2, 3};
		std::vector<vertex_t> Y = {4, 5, 6};
		std::unordered_map<vertex_t, std::unordered_set<vertex_t>> X_adj
				=
				{
						{1, {4, 5}},
						{2, {4, 5, 6}},
						{3, {5, 6}}
				};
		std::unordered_map<vertex_t, std::unordered_set<vertex_t>> Y_adj
				=
				{
						{4, {1, 2}},
						{5, {1, 2, 3}},
						{6, {2, 3}}
				};
		
		bip_graph G {permutation(X), X_adj, Y_adj};
		t.is_true(G.number_of_crossings(permutation(Y)) == 2);
	}
	//"parallel" matching, no num_crossings
	{
		std::vector<vertex_t> X = {1, 2, 3};
		std::vector<vertex_t> Y = {4, 5, 6};
		std::unordered_map<vertex_t, std::unordered_set<vertex_t>> X_adj
				=
				{
						{1, {4}},
						{2, {5}},
						{3, {6}}
				};
		std::unordered_map<vertex_t, std::unordered_set<vertex_t>> Y_adj
				=
				{
						{4, {1}},
						{5, {2}},
						{6, {3}}
				};
		bip_graph G {permutation(X), X_adj, Y_adj};
		t.is_true(G.number_of_crossings(permutation(Y)) == 0);
	}
	
	//full star on middle vertex, no num_crossings
	{
		std::vector<vertex_t> X = {1, 2, 3};
		std::vector<vertex_t> Y = {4, 5, 6};
		std::unordered_map<vertex_t, std::unordered_set<vertex_t>> X_adj
				=
				{
						{1, {}},
						{2, {4, 5, 6}},
						{3, {}}
				};
		std::unordered_map<vertex_t, std::unordered_set<vertex_t>> Y_adj
				=
				{
						{4, {2}},
						{5, {2}},
						{6, {2}}
				};
		bip_graph G {permutation(X), X_adj, Y_adj};
		t.is_true(G.number_of_crossings(permutation(Y)) == 0);
	}
	
	//K_{3,3}
	{
		std::vector<vertex_t> X = {1, 2, 3};
		std::vector<vertex_t> Y = {4, 5, 6};
		std::unordered_map<vertex_t, std::unordered_set<vertex_t>> X_adj
				=
				{
						{1, {4, 5, 6}},
						{2, {4, 5, 6}},
						{3, {4, 5, 6}}
				};
		std::unordered_map<vertex_t, std::unordered_set<vertex_t>> Y_adj
				=
				{
						{4, {1, 2, 3}},
						{5, {1, 2, 3}},
						{6, {1, 2, 3}}
				};
		bip_graph G {permutation(X), X_adj, Y_adj};
		t.is_true(G.number_of_crossings(permutation(Y)) == 9);
	}
	t.ok();
}