#include "tester.hpp"
#include "../src/permutation.hpp"

int main()
{
	Tester t;
	t.check("basic_permutation");
	{
		std::vector<int> order = {1,9,3,5,6};
		permutation p(order);
		t.is_true(p.size() == 5);
		auto it = p.begin();
		t.is_true(*it == 1);
		t.is_true(*++it == 9);
		t.is_true(*++it == 3);
		t.is_true(*++it == 5);
		t.is_true(*++it == 6);
		t.is_true(++it == p.end());
		t.is_true(p(1,9) == std::strong_ordering::less);
		t.is_true(p(9,1) == std::strong_ordering::greater);
		t.is_true(p(1,3) == std::strong_ordering::less);
		t.is_true(p(1,5) == std::strong_ordering::less);
		t.is_true(p(1,6) == std::strong_ordering::less);
		t.is_true(p(9,3) == std::strong_ordering::less);
		t.is_true(p(3,9) == std::strong_ordering::greater);
		t.is_true(p(1,1) == std::strong_ordering::equal);
		t.is_true(p(3,3) == std::strong_ordering::equal);
		t.is_true(p(9,9) == std::strong_ordering::equal);
		t.is_true(p(5,5) == std::strong_ordering::equal);
		t.is_true(p(6,6) == std::strong_ordering::equal);
		t.is_true(p.is_permutation_of(std::vector<int>({9,3,1,6,5})));
		t.is_true(p.is_permutation_of(std::vector<int>({9,3,1,5,6})));
		t.is_true(p.is_permutation_of(std::vector<int>({1,3,9,6,5})));
		t.is_true(!p.is_permutation_of(std::vector<int>({1,2,3,4,5})));
        t.ok();
	}
}