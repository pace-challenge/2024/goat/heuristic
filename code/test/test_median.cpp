#include "tester.hpp"
#include "../src/graph/bipartite_graph/graph_builder.hpp"
#include "../src/solvers/heuristic/median_solver.hpp"

int main()
{
    Tester t;
    t.check("basic test");
    {
        graph_builder b;
        b.set_fixed_part({1, 2, 3});
        b.set_free_part({4, 5, 6});
        b.add_edge(1, 4);
        b.add_edge(2, 6);
        b.add_edge(3, 5);
        auto g = b.build();
        auto res = median_solve(g);
        t.is_true(res.is_valid_result_of(g));
        t.is_true(g.number_of_crossings(permutation(res.order)) == 0);
    }
    t.ok();
}