# PACE 2024

This is the solution of the GOAT team for the exact track of the PACE 2024 challenge.
The source code is open source under the MIT License.

## Brief description

Two heuristic solvers (median and barycenter) are run sequentially and the better result is output.

## Instalation

* Modern version of GCC with C++20 support is required.

* CMake version >= 3.18

* Source code is in the `code` folder. Check the `code/README.md` for installation steps.


## Solver PDF

* TBA

